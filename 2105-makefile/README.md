# Simple Makefile

Originally posted May 1st, 2021 on [Dreamwidth](https://campanilla.dreamwidth.org/4024.html).

## Idea

We were supposed to use CodeBlocks for programming classes at my school, but CodeBlocks kept breaking on me. I switched to using emacs as my text editor, and building and testing in the command line. For practice, we have to write a lot of short C programs. I saved each under their own name so I'd be able to comeback and analyze it when needed. To skip writing many flags recommended for beginners, i wrote a short makefile with help from folks on weirder.earth.

For more background, check out the blog post.

## Prerequisites 

- [GNU make](https://gnu.org/software/make)
- [gcc](https://gnu.org/software/gcc)

## Usage 

Download the Makefile from this folder and save it into the folder with your source files.

Example of usage and output in the command line:

``` sh
$ ls
4-1-13.c    Makefile
$ make 4-1-13
gcc -Wall -Wextra -pedantic 4-1-13.c -lm -o 4-1-13
$ ls
4-1-13.c    4-1-13  Makefile
$ ./4-1-13
# code runs
```

