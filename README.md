# Blog Files 

Blog Files is a repository for all the assorted files I reference in other posts around the internet. This is to make them easy to download without any copy-paste shenanigans. The files are sorted into folders, each with it's own README file that outlines the intended usage.

## Index

- [Makefile](https://git.disroot.org/botanicals/blog-files/src/branch/main/2105-makefile) contains a simple makefile for C files.
- [Compare SHA sums](https://git.disroot.org/botanicals/blog-files/src/branch/main/2108-compare) contains a short program written in C that compares two SHA sums.
- [PF minimal](https://git.disroot.org/botanicals/blog-files/src/branch/main/2208-PF-minimal) is an installation and customization guide for a tiny Stylus theme for Pillowfort.