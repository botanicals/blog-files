# *PF minimal* Stylus Theme

This is a Stylus theme that makes minimal changes to the default Pillowfort theme. Specifically, it

- colors the sidebar numbers to be an unobtrusive blue instead of pink,
- has a commented-out option to completely remove sidebar numbers, both the top and the bottom sidebar,
- adds drop-shadow to pink elements to help them stand out a bit more.

## Installation

If you don't have it, install [Stylus](https://add0n.com/stylus.html).

Open Pillowfort and click on the Stylus icon. Tick the box in front of "Write style for:" and then click on the pillowfort.social link and it opens a new "Edit style" tab for you. Delete everything in the code-writing area and paste the contents of [PF-minimal.css](https://git.disroot.org/botanicals/blog-files/src/branch/main/2208-PF-minimal/PF-minimal.css). Edit the title of the style and click "Save."

## Customisation 

**Sidebar Numbers**

To hide the top sidebar numbers, uncomment line 15. To hide the bottom sidebar numbers, uncomment lines 28-30, ie. delete lines 27 and 31.

**Pink Element Accent**

If you don't like the way drop-shadow looks, delete lines 18-25.