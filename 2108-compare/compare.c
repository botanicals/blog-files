/* Compare by vedri
 * Available at https://git.disroot.org/botanicals/blog-files
 *
 * This program compares two strings, each in its own line,
 * of SHA sums read from a file.
 *
 * Read the projects README for more details. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void)
{
    // Declarations
    FILE *sha;
    char name[100];
    char sha1[100];
    char sha2[100];
    char c[2];

    // Collect the relevant filename
    printf("Filename: ");
    scanf("%s", name);

    // Open file
    if ((sha = fopen(name, "rt")) == NULL) {
        printf("Cannot open file!\n");
        exit(EXIT_FAILURE);
    }

    // Read the file content; run the compare function
    fscanf(sha, "%[^\n]", sha1);
    fscanf(sha, "%[\n]", c);
    fscanf(sha, "%[^ ]", sha2);

    if (strcmp(sha1, sha2) == 0)
        printf("SHA sums are the same!\n");
    else
        printf("SHA sums are not the same! Do not use.\n");

    // Close file; exit program
    fclose(sha);
    return 0;
}
