# Compare SHA Sums 

Also posted August 8th, 2021 on [Dreamwidth](https://campanilla.dreamwidth.org/4480.html).

## Idea

I wanted to practice working with files in C programs and at the same time check if the SHA sums for a Manjaro ISO file were the same.

## Prerequisites 

- [GNU make](https://gnu.org/software/make)
- [gcc](https://gnu.org/software/gcc)

## Usage 

Place the `compare.c` and `Makefile` in the same directory. Build `compare`.

``` sh
make compare
```

Either download or create a text file. The first line of the text file should contain only the SHA sum provided by the developer.

Example of `sha-test.txt`:

``` txt
6ff0ebecb9813542e1fa81bbe897bb59039bb00d 
```

Append to the file a SHA sum you generated yourself.

``` sh
sha1sum ISO-name >> sha-test.txt
```

Resulting `sha-test.txt`:

``` txt
6ff0ebecb9813542e1fa81bbe897bb59039bb00d 
6ff0ebecb9813542e1fa81bbe897bb59039bb00d ISO-name 
```

Run `compare` and enter the name of the file with SHA sums when prompted.

``` sh
$ ./compare
Filename: sha-test.txt
# The program prints whether SHA sums match or not.
```

